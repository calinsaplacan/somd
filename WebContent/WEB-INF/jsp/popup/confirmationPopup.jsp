<div class="white-popup-block mfp-hide small-dialog zoom-anim-dialog"
	id="confirmSubmitPopup">

	<div id="blackDiv">
		<h4 id="title-commentpopup">Succes</h4>

	</div>
	<div id="whiteDivDuplicatePopup">
		<br>
		<h6>Formularul a fost salvat cu succes</h6>

		<div class="row">
			<button id="ok-duplicate-bp-error" type="button"
				class="btn btn-default">OK</button>

		</div>
	</div>
</div>
