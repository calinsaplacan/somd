<%@ page language="java" contentType="text/html; charset=utf-8"
	pageEncoding="ISO-8859-1"%>
<%@ taglib uri="http://tiles.apache.org/tags-tiles" prefix="tiles"%>
<%@ taglib prefix="form" uri="http://www.springframework.org/tags/form"%>
<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core"%>



<!DOCTYPE html PUBLIC "-//W3C//DTD HTML 4.01 Transitional//EN" "http://www.w3.org/TR/html4/loose.dtd">
<html>

<script type="text/javascript">
 var serverPath = "${pageContext.request.contextPath}";
 
 //Add here further useful information needed on the client, which is available server-side
</script>
<head>
	<meta http-equiv="Content-Type" content="text/html; charset=UTF-8">
	
	<tiles:importAttribute name="jsList"/>
    <tiles:importAttribute name="cssList"/>
  	<tiles:importAttribute name="jsListDefer" ignore="true"/>
  	<tiles:importAttribute name="cssListDefer" ignore="true"/>
  	 	
	<c:forEach var="css" items="${cssList}">
        <link rel="stylesheet" type="text/css" href="${pageContext.request.contextPath}/resources/${css}">
    </c:forEach>
    
     <c:forEach var="script" items="${jsList}">
        <script src="${pageContext.request.contextPath}/resources/${script}"></script>
    </c:forEach>
    
    <c:forEach var="script" items="${jsListDefer}">
       	<script defer src="${pageContext.request.contextPath}/resources/${script}"></script>
   	</c:forEach>
   	
   	<c:forEach var="css" items="${cssListDefer}">
        <link rel="stylesheet" type="text/css" href="${pageContext.request.contextPath}/resources/${css}">
    </c:forEach>

   	<!-- HTML5 shim and Respond.js IE8 support of HTML5 elements and media queries -->
	<!--[if lt IE 9]>
	 		<script src="${pageContext.request.contextPath}/resources/js/3rdParty/html5shiv.js"></script>
			<script src="${pageContext.request.contextPath}/resources/js/3rdParty/respond.js"></script>
	<![endif]-->

</head>
<body>

	<div class="mainPage">

		<!--  add header  -->
		<div class="header">
			<tiles:insertAttribute name="header" />
		</div>
		
		<!--  add body -->
		<div class="content">
			<tiles:insertAttribute name="body" />
		</div>
	</div>

</body>
</html>