<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core"%>

<div class="bs-docs-home">

	<header class="navbar navbar-static-top bs-docs-nav" id="top"
		role="banner">

		<div class="top-menu">
			<img class="ms-logo photo-logo"
				src="${pageContext.request.contextPath}/resources/images/logo.jpg">
			<img class="ms-logo photo-logo2"
				src="${pageContext.request.contextPath}/resources/images/BAN6.jpg">

			<span class="user-log">${logedUser}</span>

		</div>

		<div style="padding-top: 2px;">
			<div class="navbar-header navbar-inverse">
				<button class="navbar-toggle collapsed" type="button"
					data-toggle="collapse" data-target="#bs-navbar"
					aria-controls="bs-navbar" aria-expanded="false">
					<div class="min-btn">
						<span class="sr-only">Toggle navigation</span> <span
							class="icon-bar"></span> <span class="icon-bar"></span> <span
							class="icon-bar"></span>
					</div>
				</button>
			</div>

			<nav id="bs-navbar"
				class="collapse navbar-collapse navbar navbar-inverse">
				<c:forEach items="${menu.childs}" var="menuChild">
					<ul class="nav navbar-nav">
						<c:choose>
							<c:when test="${menuChild.hasSubmenu == true}">

								<li class="dropdown"><a href="#" class="dropdown-toggle"
									data-toggle="dropdown"> <c:out value="${menuChild.name}"></c:out>
										<b class="caret"></b>
								</a>

									<ul class="dropdown-menu" style="display: none;">

										<c:forEach items="${menuChild.childs}" var="subMenuChild">

											<c:choose>
												<c:when test="${subMenuChild.popup == true}">
													<c:set var="url" scope="request"
														value="javascript: openPopup('aboutMenu');" />
												</c:when>

												<c:otherwise>
													<c:set var="url" scope="request"
														value="${pageContext.request.contextPath}${subMenuChild.url}" />
												</c:otherwise>
											</c:choose>

											<c:choose>
												<c:when test="${subMenuChild.hasSubmenu == true}">

													<li class="dropdown-submenu"><a href="${url}"> <c:if
																test="${not empty subMenuChild.image}">
																<img
																	src="${pageContext.request.contextPath}${subMenuChild.image}">
															</c:if> <c:out value="${subMenuChild.name}"></c:out>
													</a>
														<ul class="dropdown-menu child-menu">

															<c:forEach items="${subMenuChild.childs}"
																var="subSubMenuChild">
																<li><a
																	href="${pageContext.request.contextPath}${subSubMenuChild.url}">
																		<c:out value="${subSubMenuChild.name}"></c:out>
																</a></li>
															</c:forEach>
															<!-- end menu second level children iteration -->

														</ul></li>

												</c:when>
												<c:otherwise>
													<li><a href="${url}"> <c:if
																test="${not empty subMenuChild.image}">
																<img
																	src="${pageContext.request.contextPath}${subMenuChild.image}">
															</c:if> <c:out value="${subMenuChild.name}"></c:out>
													</a></li>
												</c:otherwise>
											</c:choose>

										</c:forEach>
										<!-- end menu first level children iteration -->

									</ul></li>

							</c:when>

							<c:otherwise>
								<li><a
									href="${pageContext.request.contextPath}${menuChild.url}"
									class="monitor-position"> <c:out value="${menuChild.name}"></c:out>
								</a></li>
							</c:otherwise>
						</c:choose>

					</ul>
				</c:forEach>
				<!-- end menu bar elements iteration -->
			</nav>
		</div>
	</header>

</div>