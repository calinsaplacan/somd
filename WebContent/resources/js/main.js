var datepickerOpts = {
		readonly:"readonly",
		todayHighlight: true,
		autoclose: true,
		todayBtn: 'linked',
	    pickerPosition: "top-right"
};

var datepickerOptsDe = {
		readonly:"readonly",
		todayHighlight: true,
		autoclose: true,
		todayBtn: 'linked',
	    pickerPosition: "top-right",
	    language: "de"
}


function CloseAndRefresh() {
	opener.location.reload(true);
	self.close();
}

function openPopup(popupId) {

	$.magnificPopup.open({

		removalDelay : 200,
		items: {
			src: popupId,
		},
		mainClass : 'my-mfp-slide-bottom',
		type: 'inline'
	});


}

//to do
$(document).on('click', '.closePopup ', function (e) {
	e.preventDefault();
	$.magnificPopup.close();
});

function openPopupAtPosition(el, popupId) {	
	$.magnificPopup.open({
		callbacks: {

			open: function() {
				var position = calculatePopupPosition(el);
				$(popupId).css({
					top: position.top ,
					left: position.left 
				}).parents('.mfp-content').addClass('mfp-contentSearch');
			}
		},
		items: {
			src: popupId,
		},
		type: 'inline'
	});
}

function closePopup(e) {
	e.preventDefault();
	$.magnificPopup.close();
}

function calculatePopupPosition(el) {

	var elClass = "." + el.split(' ')[0];
	var position = $(elClass).offset();		
	var pos = $(elClass).position();	
	var height =  $(elClass).outerHeight();
	var width =  $(elClass).outerWidth();
	var popupPosition = {"top": position.top + height, "left":  position.left + width };

	return popupPosition;
}

function isBlank(element){
	return element === null || element === undefined || element === "";
}

function callAjax(callType, callURL, callJSON, errorCallback, successCallback, hasRequestParam) {
	contentTypeValue = "application/json; charset=utf-8";
	dataToSend = JSON.stringify(callJSON);

	if (hasRequestParam) {
		contentTypeValue = "application/x-www-form-urlencoded; charset=UTF-8";
		dataToSend = callJSON;
	} 

	var result;
	$.ajax({
		cache: false,
		type : callType,
		url : window.location.origin + callURL,
		async : false,
		contentType: contentTypeValue,
		data : dataToSend
	}).success(function(data) {
		console.log("Ajax: Success");
		if (successCallback) {
			successCallback(data);
		}
		result = data;
	}).error(function(xhr, status, error) {
		console.log("Ajax: Error");

		if (errorCallback) {
			errorCallback(xhr.status, xhr.responseJSON);
		}
	}).complete(function() {
		console.log("Ajax: Complete");
	});

	return result;
}

function callAjaxRequestParam(callType, callURL, callJSON, errorCallback, successCallback) {
	callAjax(callType, callURL, callJSON, errorCallback, successCallback, true);
}


function initCallback(callBack){
	functionCallback = callBack;
}
