package ro.utcn.somd.common.web.httpfilter;

import java.io.*;
import javax.servlet.*;
import javax.servlet.http.*;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

public class GZIPFilter implements Filter {

	final Logger logger = LoggerFactory.getLogger(getClass());

	public void doFilter(ServletRequest req, ServletResponse res,
			FilterChain chain) throws IOException, ServletException {
		if (req instanceof HttpServletRequest) {
			HttpServletRequest request = (HttpServletRequest) req;
			HttpServletResponse response = (HttpServletResponse) res;
			String ae = request.getHeader("accept-encoding");
			if (ae != null && ae.indexOf("gzip") != -1) {
				logger.debug("GZIP supported, compressing.");
				GZIPResponseWrapper wrappedResponse = new GZIPResponseWrapper(response);
				try {
					chain.doFilter(req, wrappedResponse);
				}

				catch (ServletException ex) {throw ex;}
				catch (IOException ex) {throw ex;}
				catch (Exception ex) {throw new ServletException(ex);} // this is in case a Servet caused an internal error
				finally {
					wrappedResponse.finishResponse();
				}
				return;
			}
			else
			{
				logger.debug("GZIP not supported, request will be sent without compression.");
			}
			chain.doFilter(req, res);
		}
	}

	public void init(FilterConfig filterConfig) {
		// noop
	}

	public void destroy() {
		// noop
	}
}
