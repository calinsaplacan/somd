package ro.utcn.somd.common.web;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import ro.utcn.somd.common.util.ThreadLocalManager;

import org.apache.commons.logging.Log;
import org.apache.commons.logging.LogFactory;
import org.springframework.web.context.request.ServletWebRequest;
import org.springframework.web.context.request.WebRequest;

/**
 * Convenient holder class for various objects to be automatically available on thread local without invoking the various
 * services yourself
 * 
 */
public class SomdRequestContext {

	protected static final Log LOG = LogFactory.getLog(SomdRequestContext.class);
	private static final ThreadLocal<SomdRequestContext> S5_REQUEST_CONTEXT = ThreadLocalManager.createThreadLocal(SomdRequestContext.class);
	protected HttpServletRequest request;
	protected HttpServletResponse response;
	protected WebRequest webRequest;


	public static SomdRequestContext getS5RequestContext() {
		return S5_REQUEST_CONTEXT.get();
	}

	public static void setS5RequestContext(SomdRequestContext somdRequestContext) {
		S5_REQUEST_CONTEXT.set(somdRequestContext);
	}

	public HttpServletRequest getRequest() {
		return request;
	}

	/**
	 * Sets the current request on the context. 
	 * 
	 * @param request
	 */
	public void setRequest(HttpServletRequest request) {
		this.request = request;
		this.webRequest = new ServletWebRequest(request);
	}

	/**
	 * Returns the response for the context
	 * 
	 * @return
	 */
	public HttpServletResponse getResponse() {
		return response;
	}

	/**
	 * Sets the response on the context
	 * 
	 * @param response
	 */
	public void setResponse(HttpServletResponse response) {
		this.response = response;
	}

	/**
	 * Sets the generic request on the context. This is available to be used in non-Servlet environments (like Portlets).
	 * @param webRequest
	 */
	public void setWebRequest(WebRequest webRequest) {
		this.webRequest = webRequest;
		if (webRequest instanceof ServletWebRequest) {
			this.request = ((ServletWebRequest) webRequest).getRequest();
			setResponse(((ServletWebRequest) webRequest).getResponse());
		}
	}

	/**
	 * Returns the generic request for use outside of servlets (like in Portlets).
	 * 
	 * @return the generic request
	 */
	public WebRequest getWebRequest() {
		return webRequest;
	}
}
