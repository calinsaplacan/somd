package ro.utcn.somd.common.web;

import java.util.Locale;

import javax.servlet.http.HttpServletRequest;

import org.apache.tiles.request.servlet.ServletRequest;
import org.springframework.web.context.request.RequestContextHolder;
import org.springframework.web.context.request.ServletRequestAttributes;
import org.springframework.web.servlet.support.RequestContextUtils;

public class LocaleResolver {
	
	
    public static Locale getLocale() {
        HttpServletRequest request = ((ServletRequestAttributes) RequestContextHolder
        		.getRequestAttributes()).getRequest();
        return getLocale(request);
    }

    public static Locale getLocale(HttpServletRequest request) {
        return (Locale) RequestContextUtils.getLocale((HttpServletRequest) request);
    }

    
}
