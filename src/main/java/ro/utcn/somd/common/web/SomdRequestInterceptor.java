package ro.utcn.somd.common.web;

import javax.annotation.Resource;

import org.springframework.ui.ModelMap;
import org.springframework.web.context.request.WebRequest;
import org.springframework.web.context.request.WebRequestInterceptor;

/**
 * <p>Interceptor responsible for setting up the S5RequestContext for the life of the request. This interceptor
 * should be the very first one in the list, as other interceptors might also use {@link SomdRequestContext}.</p>
 * 
 * 
 */
public class SomdRequestInterceptor implements WebRequestInterceptor{


	@Resource(name = "somdRequestProcessor")
	protected SomdRequestProcessor requestProcessor;

	@Override
	public void preHandle(WebRequest request) throws Exception {
		requestProcessor.process(request);
	}

	@Override
	public void postHandle(WebRequest request, ModelMap model) throws Exception {
		if (model != null) {
			model.put("menu", MenuResolver.getMenu());
			model.put("logedUser", MenuResolver.userLogat);
		
		}
	}

	@Override
	public void afterCompletion(WebRequest request, Exception ex) throws Exception {
		requestProcessor.postProcess(request);
	}
}
