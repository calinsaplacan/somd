package ro.utcn.somd.common.web;
import javax.servlet.http.HttpServletRequest;

import ro.utcn.somd.web.util.menu.Menu;

import org.springframework.web.context.request.ServletWebRequest;
import org.springframework.web.context.request.WebRequest;

public class MenuResolver {

	
	   public static String menuRequestAttributeName = "menu";
	   public static String userLogat;

	    public static Menu getMenu(HttpServletRequest request) {
	        return getMenu(new ServletWebRequest(request));
	    }
	    
	    public static Menu getMenu() {
	        WebRequest request = SomdRequestContext.getS5RequestContext().getWebRequest();
	        return getMenu(request);
	    }

	    public static Menu getMenu(WebRequest request) {
	        return (Menu) request.getAttribute(menuRequestAttributeName, WebRequest.SCOPE_REQUEST);
	    }

	    public static void setMenu(Menu menu) {
	        WebRequest request = SomdRequestContext.getS5RequestContext().getWebRequest();
	        request.setAttribute(menuRequestAttributeName, menu, WebRequest.SCOPE_REQUEST);
	    }




}
