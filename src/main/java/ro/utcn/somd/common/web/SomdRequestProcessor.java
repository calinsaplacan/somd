package ro.utcn.somd.common.web;

import org.springframework.stereotype.Component;
import org.springframework.web.context.request.WebRequest;

import ro.utcn.somd.common.util.ThreadLocalManager;
import ro.utcn.somd.web.util.menu.MenuBuilder;

/**
 * Generic class that should be used for processing requests from Servlet Filters or Spring Interceptors
 */
@Component("somdRequestProcessor")
public class SomdRequestProcessor {


	/**
	 * Process the current request. Examples would be setting the currently logged in user on the request.
	 * 
	 * @param request
	 */
	public void process(WebRequest request) {
		SomdRequestContext requestContext = new SomdRequestContext();
		requestContext.setWebRequest(request);
		SomdRequestContext.setS5RequestContext(requestContext);
		
		MenuResolver.setMenu(MenuBuilder.getMainMenu());

	}

	/**
	 * Should be called if work needs to be done after the request has been processed.
	 * 
	 * @param request
	 */
	public void postProcess(WebRequest request) {
		ThreadLocalManager.remove();

	}

}
