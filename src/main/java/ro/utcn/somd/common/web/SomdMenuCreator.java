package ro.utcn.somd.common.web;

import javax.annotation.Resource;

import org.springframework.ui.ModelMap;
import org.springframework.web.context.request.WebRequest;
import org.springframework.web.context.request.WebRequestInterceptor;

import ro.utcn.somd.web.util.menu.MenuBuilder;

public class SomdMenuCreator implements WebRequestInterceptor{


	@Resource(name = "somdRequestProcessor")
	protected SomdRequestProcessor requestProcessor;

	@Override
	public void preHandle(WebRequest request) throws Exception {
		requestProcessor.process(request);
	}

	@Override
	public void postHandle(WebRequest request, ModelMap model) throws Exception {
		if (model != null) {
			model.put("menu", MenuBuilder.getMainMenu());

		}
	}

	@Override
	public void afterCompletion(WebRequest request, Exception ex) throws Exception {
		requestProcessor.postProcess(request);
	}
}
