package ro.utcn.somd.web.util.menu;

import java.util.List;


/**
 * Design Pattern: Composite
 *
 * Node of a Menu structure.
 *
 * @author Rp
 */
public abstract class MenuNode implements java.io.Serializable {

    private static final long serialVersionUID = 2263832338038983931L;

    protected String menuId;
    private String name ;
    private String image;
    private boolean hasSubmenu;

    private int level;
    private String url;
    private boolean popup;

    private MenuNode parent = null;
    boolean marked = false;

    public abstract MenuNode addChild(MenuNode node);
    public abstract MenuNode removeChild(MenuNode node);

    public abstract boolean hasChildren();
    public abstract List<MenuNode> getChilds();
    public abstract MenuNode getChild(String menuId);


    public void setParent(MenuNode node) {
        parent = node;
        parent.addChild(this);
    }

    public MenuNode getParent() {
        return parent;
    }


    public String getMenuId() {
        return menuId;
    }

    public void setMenuId(String menuId) {
        this.menuId = menuId;
    }

    public String getPath(){

        return getMenuId();
    }
    public void setPath(String path){
        setMenuId(path);
    }
    public String getName() {
        return name;
    }
    public String getImage(){
        return image;
    }
    public void setImage(String aImage){
        image=aImage;
    }
    public void setName(String stringId) {
        this.name = stringId;
    }

    public boolean isMarked() {
        return marked;
    }

    public int getLevel() {
        return level;
    }

    public void setLevel(int level) {
        this.level = level;
    }

    public String getUrl() {
        return url;
    }

    public void setUrl(String url) {
        this.url = url;
    }


    public boolean isPopup() {
        return popup;
    }

    public void setPopup(boolean popup) {
        this.popup = popup;
    }

    /**
     * Compare this MenuNode with an Object. The comparison is done by comparing
     * the properties String ID and Menu ID.
     *
     * @param anObject object the Menu node should compared with.
     * @return true if the Object is equal to this MenuNode; false otherwise.
     */
    public boolean equals(Object anObject) {
        if (!(anObject instanceof MenuNode)) {
            return false;
        }

        MenuNode anNode = (MenuNode) anObject;
        if (anNode.menuId == menuId && anNode.name == name) {
            return true;
        }

        return false;
    }
	public boolean isHasSubmenu() {
		return hasSubmenu;
	}
	public void setHasSubmenu(boolean hasSubmenu) {
		this.hasSubmenu = hasSubmenu;
	}

}