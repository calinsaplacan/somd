package ro.utcn.somd.web.util.menu;

public class MenuBuilder {

	private static String IMAGE_PATH = "/resources/images/olds4.jpg";
	private static String SEARCH_URL = "/search";

	public static Menu getMainMenu() {

		Menu mainMenu = new Menu("mainMenu");

		Menu infoMenu = new Menu("infoMenu");
		infoMenu.setName("Info");
		infoMenu.setParent(mainMenu);
		infoMenu.setHasSubmenu(true);

		Menu specificatiiProiect = new Menu("specificatiiProiect");
		specificatiiProiect.setName("Specificatii Proiect");
		specificatiiProiect.setParent(infoMenu);
		specificatiiProiect.setHasSubmenu(true);
		

		Menu specificatiiDetaliiProiect = new Menu("specificatiiDetaliiProiect");
		specificatiiDetaliiProiect.setName("Detalii Proiect");
		specificatiiDetaliiProiect.setParent(specificatiiProiect);
		specificatiiDetaliiProiect.setHasSubmenu(true);
		specificatiiDetaliiProiect.setUrl("/resources/SpecificatieProiect.pdf");

		Menu searchMenu = new Menu("searchMenu");
		searchMenu.setName("Cautare");
		searchMenu.setParent(mainMenu);
		searchMenu.setHasSubmenu(true);

		Menu searchPacentiMenu = new Menu("searchPacentiMenu");
		searchPacentiMenu.setName("Cautare Pacienti");
		searchPacentiMenu.setParent(searchMenu);
		searchPacentiMenu.setHasSubmenu(false);
		searchPacentiMenu.setUrl("/searchPacienti");

		Menu searchMediciMenu = new Menu("searchMediciMenu");
		searchMediciMenu.setName("Cautare Medici");
		searchMediciMenu.setParent(searchMenu);
		searchMediciMenu.setHasSubmenu(false);

		Menu analizeMenu = new Menu("analizeMenu");
		analizeMenu.setName("Analize");
		analizeMenu.setParent(mainMenu);
		analizeMenu.setHasSubmenu(false);
		analizeMenu.setUrl("/analizePage");

		Menu programariMenu = new Menu("programariMenu");
		programariMenu.setName("Programari");
		programariMenu.setParent(mainMenu);
		programariMenu.setHasSubmenu(false);
		programariMenu.setUrl("/programariPage");
		

		Menu pacientiMenu = new Menu("pacientiMenu");
		pacientiMenu.setName("Pacienti");
		pacientiMenu.setParent(mainMenu);
		pacientiMenu.setHasSubmenu(true);

		Menu editarePacientiSubmenu = new Menu("editarePacientiSubmenu");
		editarePacientiSubmenu.setName("Editare pacienti");
		editarePacientiSubmenu.setParent(pacientiMenu);
		editarePacientiSubmenu.setHasSubmenu(false);
		editarePacientiSubmenu.setUrl("/editPacienti");

		Menu mediciMenu = new Menu("mediciMenu");
		mediciMenu.setName("Medici");
		mediciMenu.setParent(mainMenu);
		mediciMenu.setHasSubmenu(true);

		Menu programMediciSubmenu = new Menu("programMediciSubmenu");
		programMediciSubmenu.setName("Program Medici");
		programMediciSubmenu.setParent(mediciMenu);
		programMediciSubmenu.setHasSubmenu(false);
		programMediciSubmenu.setUrl("/programMedici");

		Menu detaliiMediciSubmenu = new Menu("detaliiMediciSubmenu");
		detaliiMediciSubmenu.setName("Detalii Medici");
		detaliiMediciSubmenu.setParent(mediciMenu);
		detaliiMediciSubmenu.setHasSubmenu(false);

		Menu settingsMenu = new Menu("settingsMenu");
		settingsMenu.setName("Setari");
		settingsMenu.setParent(mainMenu);
		settingsMenu.setHasSubmenu(false);

		Menu aboutMenu = new Menu("aboutMenu");
		aboutMenu.setName("Despre");
		aboutMenu.setParent(mainMenu);
		aboutMenu.setHasSubmenu(false);
		aboutMenu.setPopup(true);

		return mainMenu;

	}

}
