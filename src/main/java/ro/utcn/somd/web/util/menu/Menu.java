package ro.utcn.somd.web.util.menu;

import java.util.ArrayList;
import java.util.Iterator;
import java.util.List;

public class Menu extends MenuNode implements java.io.Serializable {

    private static final long serialVersionUID = -8685446124337128486L;
    private List<MenuNode> children = null;

    public Menu(String menuId) {
        children = new ArrayList<>();
        this.menuId = menuId;
    }

    public MenuNode addChild(MenuNode node) {
        children.add(node);
        return node;
    }

    public MenuNode removeChild(MenuNode aNode) {
        MenuNode menuNode = null;
        Iterator<MenuNode> itrChildren = children.iterator();
        
        while (itrChildren.hasNext()) {
        	menuNode = (MenuNode) itrChildren.next();
        	
        	if (menuNode.equals(aNode)) {
        		return children.remove(children.indexOf(aNode));
        	}
        	
        	if (menuNode instanceof Menu) {
        		menuNode.removeChild(aNode);
        	}
        }

        return menuNode;
    }


    public MenuNode getChild(String menuId) {
        MenuNode node = null;

        if (this.menuId.equals(menuId)) {
            return this;
        }
        
        for (MenuNode menuNode : children) {
        	if (node == null) {
        		node = menuNode.getChild(menuId);
        	} else {
        		break;
        	}
        }

        return node;
    }
   
    public List<MenuNode> getChilds() {
    	return children;
    }

    public boolean hasChildren() {
        return children.size() > 0 ? true : false;
    }
     

}
