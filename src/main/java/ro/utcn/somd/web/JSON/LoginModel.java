package ro.utcn.somd.web.JSON;

public class LoginModel {

	String username;
	String password;
	String remeberMe;

	public LoginModel(String username, String password, String remeberMe) {
		super();
		this.username = username;
		this.password = password;
		this.remeberMe = remeberMe;
	}

	public LoginModel() {
		super();
		// TODO Auto-generated constructor stub
	}

	public String getUsername() {
		return username;
	}

	public void setUsername(String username) {
		this.username = username;
	}

	public String getPassword() {
		return password;
	}

	public void setPassword(String password) {
		this.password = password;
	}

	public String getRemeberMe() {
		return remeberMe;
	}

	public void setRemeberMe(String remeberMe) {
		this.remeberMe = remeberMe;
	}

	@Override
	public String toString() {
		return "LoginModel [username=" + username + ", password=" + password + ", remeberMe=" + remeberMe + "]";
	}

}
