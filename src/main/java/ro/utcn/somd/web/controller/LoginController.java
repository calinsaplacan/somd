package ro.utcn.somd.web.controller;

import ro.utcn.somd.web.JSON.LoginModel;
import ro.utcn.somd.common.web.MenuResolver;

import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.web.bind.annotation.ModelAttribute;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;

@Controller
public class LoginController {

	@RequestMapping(value = "/login", method = RequestMethod.GET)
	public String getSearchView(Model model) {

		return "login";
	}

	@RequestMapping(value = "/loginClick", method = RequestMethod.POST)
	public String getBpInfo(@ModelAttribute("loginModel") LoginModel loginModel, Model model) {
		if (loginModel.getUsername().equals((String) "medic")) {
			if (loginModel.getPassword().equals((String) "medic")) {
				MenuResolver.userLogat="Medic";
				model.addAttribute("logedUser", "Medic");
				return "news-page";
			}

		}
		if (loginModel.getUsername().equals((String) "admin")) {
			if (loginModel.getPassword().equals((String) "admin")) {
				model.addAttribute("logedUser", "Admin");
				MenuResolver.userLogat="Admin";
				return "news-page";
			}

		}
		if (loginModel.getUsername().equals((String) "pacient")) {
			if (loginModel.getPassword().equals((String) "pacient")) {
				model.addAttribute("logedUser", "Pacient");
				MenuResolver.userLogat="Pacient";
				return "news-page";
			}

		}

		return "login";
	}
}
