package ro.utcn.somd.web.utils;

import java.net.MalformedURLException;
import java.net.URI;
import java.net.URISyntaxException;
import java.net.URL;

import javax.servlet.http.HttpServletRequest;

public class HttpUtils {


	public static String makeUrlAbsolute(final URL absoluteBaseUrl, String relativeUrl) throws URISyntaxException{
		boolean isAbsoluteUrl = relativeUrl.toLowerCase().startsWith("http:") ||
				relativeUrl.toLowerCase().startsWith("https:");
		if (!isAbsoluteUrl)
		{
			final StringBuffer url = new StringBuffer();

			url.append(absoluteBaseUrl.getProtocol());
			url.append("://");
			url.append(absoluteBaseUrl.getHost());
			if (absoluteBaseUrl.getPort() != -1)
			{
				// Only add port if not default
				if ("http".equals(absoluteBaseUrl.getProtocol()))
				{
					if (absoluteBaseUrl.getPort() != 80)
					{
						url.append(":").append(absoluteBaseUrl.getPort());
					}
				}
				else if ("https".equals(absoluteBaseUrl.getProtocol()))
				{
					if (absoluteBaseUrl.getPort() != 443)
					{
						url.append(":").append(absoluteBaseUrl.getPort());
					}
				}

			}

			final URI lUri = new URI(relativeUrl);
			url.append(lUri.getPath());

			return url.toString();
		}
		else
		{
			return relativeUrl;
		}
	}

	public static String makeUrlAbsolute(HttpServletRequest req, String originalUrl) throws MalformedURLException, URISyntaxException{
		StringBuffer url = req.getRequestURL();
		String requestURL = url.toString();
		final URL lUrl = new URL(requestURL);
		return HttpUtils.makeUrlAbsolute(lUrl, originalUrl);
	}
}
