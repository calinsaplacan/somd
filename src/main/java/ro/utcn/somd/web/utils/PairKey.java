package ro.utcn.somd.web.utils;

import java.io.Serializable;

public final class PairKey<A, B> implements Serializable {
	private  A key;
	private  B value;
	public A getKey() {
		return key;
	}

	public B getValue() {
		return value;
	}

	
	public PairKey(A aUser, B anIdentifier) {
		key = aUser;
		value = anIdentifier;
	}

	public static <A, B> PairKey<A, B> make(A a, B b) {
		return new PairKey<A, B>(a, b);
	}

	public int hashCode() {
		return (key != null ? key.hashCode() : 0) + 31 * (value != null ? value.hashCode() : 0);
	}

	public boolean equals(Object o) {
		if (o == null || o.getClass() != this.getClass()) {
			return false;
		}
		PairKey<?, ?> that = (PairKey<?, ?>) o;
		return (key == null ? that.key == null : key.equals(that.key))
				&& (value == null ? that.value == null : value.equals(that.value));
	}

	public void setKey(A key) {
		this.key = key;
	}

	public void setValue(B value) {
		this.value = value;
	}
	
	
}